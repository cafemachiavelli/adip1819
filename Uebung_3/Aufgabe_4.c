#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void calcb() {
  float x1 = 10000.0;
  float x2 = -1.0e-3 / 9.0;
  float x3 = 25.0e2;
  float x4 = 1.0e-3 / 7.0;
  float x5 = -12.5e3;
  float result = 0;
  result = x1 + x2 + x3 + x4 + x5;
  printf("%f\n", result);
}

void calcc() {
  double x1 = 10000.0;
  double x2 = -1.0e-3 / 9.0;
  double x3 = 25.0e2;
  double x4 = 1.0e-3 / 7.0;
  double x5 = -12.5e3;
  double result = 0;
  result = x1 + x2 + x3 + x4 + x5;
  printf("%lf\n", result);
}

void calcd() {
  float x[5] = {10000.0, -1.0e-3 / 9.0, 25.0e2, 1.0e-3 / 7.0, -12.5e3};
  float s_alt, s = 0, d = 0;
  for (int i = 0; i < 5; i++) {
    s_alt = s;
    s += x[i];
    d += x[i] - (s - s_alt);
  }
  s += d;
  printf("%f\n", s);
}

//e: Differenzen, die aufgrund der beschränkten Präzision des Zahlenformats weggerundet werden,
// werden in D summiert und bleiben daher erhalten. Da S am Ende = 0 ist, kann der Inhalt von D
// dann wieder in S aufgenommen werden.

//f: Es ändert sich am Ergebnis nichts, da auch kleine Differenzen nun noch ins Zahlenformat passen.
// D ist somit überflüssig und beeinflusst die Summe nicht mehr.


int main() { 
    calcb();
    calcc();
    calcd();
    calcf();
    return 0; }