#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

int str_len(char *str) {
  int len = 0;
  while (str[len]) {
    len += 1;
  }
  return len + 1;
}

char *str_rev(char *str) {
  int len = str_len(str);
  char *str2 = malloc(len * sizeof(char));
  for (int i = 0; i < len - 1; i++) {
    str2[i] = str[len - 2 - i];
  }
  str2[len] = '\0';
  return str2;
}

int palindrome(char *str) {
  int len = str_len(str);
  for (int i = 0; i < len / 2; i++) {
    if (toupper(str[i]) != toupper(str[len - 2 - i]))
      return 0;
  }
  return 1;
}

char* encrypt(char *str, int k) {
  int len = str_len(str);
  char* dest = malloc(len*sizeof(char));
  for (int i = 0; i < len; i++) {
    if (str[i] >= 'A' && str[i] <= 'Z') {
     dest[i] = ((str[i] - 'A') + k) % ('Z' - 'A' + 1) + 'A';
    }

    if (str[i] >= 'a' && str[i] <= 'z') {
      dest[i] = ((str[i] - 'a') + k) % ('z' - 'a' + 1) + 'a';
    }
  }
  return dest;
}

char *decrypt(char *str, int k) { 
    return encrypt(str, 26-k);
         }

int main() {

  char* string = "Teset";
  printf("%s\n",str_rev(string));
  printf("%d\n",palindrome(string));
  printf("%s\n", encrypt(string, 13));
  
}