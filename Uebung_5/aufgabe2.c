#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct DynArray {
  int maxsize;
  int cursize;
  int *array;
} DynArray;

void dyn_array_add(DynArray *a, int n) {
  a->cursize++;
  if (a->cursize > a->maxsize) {
    int *temp = a->array;
    a->array = malloc(2 * a->maxsize * sizeof(int)); // double array
    for (int i = 0; i < a->cursize; i++) {           // copy
      a->array[i] = temp[i];
    }
    a->maxsize *=2;
    free(temp);
  }
  a->array[a->cursize - 1] = n;
}

void dyn_array_min_add(DynArray *a, int n) {
  a->cursize++;
  if (a->cursize > a->maxsize) {
    int *temp = a->array;
    a->array = malloc((a->maxsize + 1) * sizeof(int)); // double array
    for (int i = 0; i < a->cursize; i++) {             // copy
      a->array[i] = temp[i];
    }
    a->maxsize ++;
    free(temp);
  }
  a->array[a->cursize - 1] = n;
  ;
}

void printDynArray(DynArray *a) {
  for (int i = 0; i < a->cursize; i++) {
    printf("%3d", a->array[i]);
  }
  printf("\n");
}

int main() {
  DynArray *test;
  test->maxsize = 1;
  test->cursize = 0;
  test->array = malloc(5 * sizeof(int));
  dyn_array_add(test, 5);
  dyn_array_add(test, 5);
  dyn_array_add(test, 5);
  printDynArray(test);
  printf("Used: %i\n", test->cursize);
  printf("Size: %i\n", test->maxsize);
  return 0;
  free(test->array);
}