#include <stdio.h>
#include <stdlib.h>

typedef struct DoubleNode {
  struct DoubleNode *next;
  double value;
} DoubleNode;

DoubleNode *insertFirst(DoubleNode *head_ref, int new_data) {
  DoubleNode *new_node = malloc(sizeof(DoubleNode));
  new_node->value = new_data;
  new_node->next = head_ref;
  return new_node;
}

void printList(DoubleNode *head) {
  DoubleNode *cursor = head;
  printf("(");
  while (cursor != NULL) {
    printf("%f", cursor->value);
    printf(" ");
    cursor = cursor->next;
  }
  printf(")\n");
}

DoubleNode *insertLast(DoubleNode *head, int new_data) {
  DoubleNode *tmp = head;
  while (tmp->next != NULL) {
    tmp = tmp->next;
  }
  DoubleNode *new_node = malloc(sizeof(DoubleNode));
  new_node->value = new_data;
  tmp->next = new_node;
  return head;
}

DoubleNode *reverseDoubleListCon(DoubleNode *head) {
  DoubleNode *rev = NULL, *tmp;
  while (head) {
    tmp = malloc(sizeof(DoubleNode));
    tmp->value = head->value;
    tmp->next = rev;
    rev = tmp;
    head = head->next;
  }
  return rev;
}
DoubleNode *reverseDoubleList(DoubleNode *head) {
  DoubleNode *rev = NULL, *next;
  while (head) {
    next = head->next; // put next cursor on next element in list
    head->next = rev;  // append previous element to list
    rev = head;        // center head of new list on position
    head = next;       // iterate
  }
  return rev;
}

double get(DoubleNode *head, int index) {
  for (int i = 0; i < index; i++) {
    if (!head->next) {
      printf("Error: List does not contain %i elements.\n", index + 1);
      return 0;
    }
    head = head->next;
  }
  return head->value;
}
void delete (DoubleNode **head, int index) {
  DoubleNode *tmp;
  for (int i = 0; i < index; i++) {
    head = &(*head)->next;
    if (!*head) {
      printf("Error: List does not contain %i elements.\n", index + 1);
      return;
    }
  }
  tmp = *head;
  *head = (*head)->next;
  free(tmp);
}
void insert(DoubleNode **head, double value, int index) {
int end = 0;
  for (int i = 0; i < index; i++) {
    head = &(*head)->next;
    if (end) {
      printf("Error: List does not contain %i elements.\n", index + 1);
      return;
    }
    if (!*head) {
      end = 1;
    }
  }
  DoubleNode *new_node = malloc(sizeof(DoubleNode));
  new_node->value = value;
  new_node->next = *head;
  *head = new_node;

  return;
}

int main() {
  DoubleNode *L1, *L2;
  L1 = insertFirst(L1, 5);
  L1 = insertFirst(L1, 4);
  L1 = insertLast(L1, 6);
  printList(L1);
  L2 = reverseDoubleListCon(L1);
  printList(L1);
  printList(L2);
  L1 = reverseDoubleList(L1);
  printList(L1);
  L1 = insertFirst(L1, 7);
  L1 = insertFirst(L1, 8);
  printf("%f\n", get(L1, 0));
  printf("%f\n", get(L1, 4));
  printf("%f\n", get(L1, 5));
  printList(L1);
  delete (&L1, 4);
  printList(L1);
  delete (&L1, 0);
  printList(L1);
  insert(&L1, 4.5, 3);
  printList(L1);
  insert(&L1, 9, 0);
  printList(L1);
}