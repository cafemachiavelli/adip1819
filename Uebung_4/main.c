#include "main.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char **argv) {
  if (argc < 2) {
    fprintf(stderr, "\nVerwendung: %s (1-6)\n\n", argv[0]);
    return 0;
  }
  switch (atoi(argv[1])) {
  case 1:
    printf("5c2 ist %i\n", binomial(9, 4));
    break;
  case 2:
    printf("2 plus 0 ist %i\n", add(2, 0));
    printf("2 minus 0 ist %i\n", sub(2, 0));
    printf("-3 plus 2 ist %i\n", add(-3, 2));
    printf("-3 minus 2 ist %i\n", sub(-3, 2));
    printf("2 mal 0 ist %i\n", mult(2, 0));
    printf("-3 mal 2 ist %i\n", mult(-3, 2));
    break;
  case 3:
    trig();
    break;
  case 4:
    findPyt(20);
    break;
  case 5:
    getChange();
  }
  return 0;
}

// -------- Aufgabe 1 ---------

int factorial(int n) {
  int result = 1;
  for (int i = n; i > 1; i--) {
    result *= i;
  }
  return result;
}

int binomial(int n, int k) {
  return n < k ? 0 : factorial(n) / (factorial(k) * factorial(n - k));
}

int lottery(int n, int k) { return binomial(n, k) * factorial(k); }

// -------- Aufgabe 2 ---------
int succ(int x) { return ++x; }

int pre(int x) { return --x; }

int add(int x, int y) {
  if (y == 0)
    return x;
  else if (y > 0)
    return add(succ(x), pre(y));
  else
    return add(pre(x), succ(y));
}

int sub(int x, int y) {
  if (y == 0)
    return x;
  return sub(pre(x), pre(y));
}

int mult(int x, int y) {
  if (y == 0)
    return 0;
  return add(x, mult(x, pre(y)));
}

// -------- Aufgabe 3 ---------

void trig() {
  float x;
  printf("Bitte Zahl eingeben.\n");
  scanf("%f", &x);
  float sin = 0, cos = 0, exp = 0;
  for (int n = 0; n <= 5; n++) {
    sin += pow(-1, n) * pow(x, 2 * n + 1) / factorial(2 * n + 1);
    cos += pow(-1, n) * pow(x, 2 * n) / factorial(2 * n);
  }
  for (int n = 0; n <= 10; n++) {
    exp += pow(x, n) / factorial(n);
  }
  printf("Sinus von %f: %f\n", x, sin);
  printf("Kosinus von %f: %f\n", x, cos);
  printf("e hoch %f: %f\n", x, exp);
}

// -------- Aufgabe 4 ---------

void findPyt(int limit) {
  int a, b, c, aa, bb, cc;
  for (a = 0; a <= limit; a++) {
    aa = a * a;
    for (b = a + 1; b <= limit; b++) {
      bb = b * b;
      for (c = b + 1; c <= limit; c++) {
        cc = c * c;
        if (aa + bb == cc)
          printf("Found triple: %i %i %i\n", a, b, c);
      }
    }
  }
}

// -------- Aufgabe 5 ---------

void getChange() {
  srand(time(NULL));
  int payment, bill = rand() % 500;
  int counter;
  printf("Faellig: %i.\nWieviel wurde gegeben?\n", bill);
  scanf("%i", &payment);
  int diff = payment - bill;
  int coins[6] = {50, 20, 10, 5, 2, 1};
  for (int i = 0; i < 6; i++) {
    // printf("test %i %i")
    counter = 0;
    while (diff >= coins[i]) {
      diff -= coins[i];
      counter++;
    }
    printf("Es werden %i %ier-Muenzen ausgegeben.\n", counter, coins[i]);
  }
}